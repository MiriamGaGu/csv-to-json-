const fs = require ('fs')
const http = require('http');
const hostname = '127.0.0.1';
const port = 3001;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
  res.end('JSON data on terminal');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});


const csvFilePath='./customer-data.csv'
const csv=require('csvtojson')
csv()
.fromFile(csvFilePath)
.then((jsonObj)=>{

    let json_Obj = JSON.stringify({data:jsonObj});

    fs.writeFile("./file.json", json_Obj, (err) => {
      if(err) throw err;
    });

    fs.readFile("./file.json", "utf8", (err, data) => {
      if(err) throw err;
      let obj = JSON.parse(data)

      obj.data.forEach(i => console.log(i.id))
    })

})

